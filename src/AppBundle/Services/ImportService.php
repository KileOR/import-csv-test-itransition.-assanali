<?php
namespace AppBundle\Services;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Modifier\MapIterator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class ImportService
{


    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array|Product[]
     */
    private $products = [];

    /**
     * @var EntityPrepareService
     */
    private $entityPrepareService;

    /**
     * @var int
     */
    private $max_rows;

    /**
     * ImportService constructor.
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     * @param EntityPrepareService $entityPrepareService
     * @param int $max_rows
     */

    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator,
        LoggerInterface $logger,
        EntityPrepareService $entityPrepareService,
        $max_rows
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->entityPrepareService = $entityPrepareService;
        $this->max_rows = $max_rows;
    }


    /**
     * @param MapIterator $csvResult
     * @param bool $isTest
     * @return array|int
     */

    public function importProducts(MapIterator $csvRows, bool $isTest): array
    {
        $savedRows = 0;
        $results = [
            'success'=> 0,
            'validate_error'=> 0,
            'rows_numbers'=>0
        ];


        foreach ($csvRows as $row) {
            $results['rows_numbers']++;
            //Try preapre product for entity in entity prepare service by row
            try {
                $product = $this->entityPrepareService->prepareEntity($row);
            } catch (Exception $e) {
                $this->logger->error(sprintf("%s\nat row %d", $e->getMessage(), $row));
                continue;
            }

            //Validate contrains from product
            $errors = $this->validator->validate($product);

            if (count($errors) === 0) {
                //Check is there any product with same Code
                if (array_key_exists($product->getProductCode(), $this->products)) {
                    // Delete product with same code
                    $this->em->remove($product);
                } else {
                    //save products
                    $this->products[$product->getProductCode()]=$product;
                    $this->em->persist($product);
                    $savedRows++;
                    $results['success']++;
                }
            } else {
                $this->logger->error(sprintf("%s\nat Product: %s", (string)$errors, $product->getProductName()));
                $results['validate_error']++;
            }
            if (!$isTest) { //Check is a test now
                if ($savedRows == $this->max_rows) { //Flush after 50 rows
                    $this->em->flush();
                    $savedRows = 0;
                }
            }
        }
        if (!$isTest) {
            $this->em->flush();
        }

        return $results;
    }
}
