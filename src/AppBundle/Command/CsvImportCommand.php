<?php

namespace AppBundle\Command;

use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CsvImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('csv:import')
            ->setDescription('Command for import rows from csv to database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Wait for importing');

        //Read from csv file by League/csv library
        $reader = Reader::createFromPath('%kernel.root_dir%/../src/AppBundle/Data/stock.csv');
        //return Assoviative array
        $csvRows = $reader->fetchAssoc();

        $importService = $this->getContainer()->get('import_service');


        //Import from csv reader
        $results = $importService->importProducts($csvRows, false);


        $io->success(sprintf("Data imported: %s \n Not imported: %s", $results['success'], $results['validate_error']));
    }
}
