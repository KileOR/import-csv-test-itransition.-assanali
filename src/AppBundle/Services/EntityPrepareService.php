<?php
namespace AppBundle\Services;

use AppBundle\Entity\Product;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Yaml\Yaml;

class EntityPrepareService
{

    /**
     * @param array $row
     * @return Product
     * @throws Exception
     */
    public function prepareEntity(array $row):Product
    {
        //read keys from YML file
        $keysMapping = Yaml::parse(file_get_contents('%kernel.root_dir%/../src/AppBundle/Data/keysForRows.yml'));
        $product_name = $row[$keysMapping['product_name']];
        $product_code = $row[$keysMapping['product_code']];
        $product_desc = $row[$keysMapping['product_desc']];
        $product_cost = (float)$row[$keysMapping['product_cost']];
        $product_stock = (int)$row[$keysMapping['product_stock']];

        //Check is there 6 collums
        if (count($row)!== count($keysMapping)) {
            $message = sprintf("Wrong count of columns.\nExpected %d, given %d", count($keysMapping), count($row));
            throw new Exception($message);
        }


        // Check is there correct format for Cost,Code and Stock
        if (preg_match('/\W/', (string)$product_code)) {
            throw new Exception(sprintf("Incorrect product code: %s in %s", $product_code, $product_name));
        }
        if (!preg_match('/^\d+$/', (string)$product_stock)) {
            throw new Exception(sprintf("Incorrect stock size: %s in %s", (string)$product_stock), $product_name);
        }
        if (!preg_match('/^\d{1,}(\.\d{1,2})?$/', (string)$product_cost)) {
            throw new Exception(sprintf("Incorrect cost of product: %s in %s", (string)$product_cost, $product_name));
        }


        //set up product
        $product = (new Product())
            ->setProductName($product_name)
            ->setProductDesc($product_desc)
            ->setProductCode($product_code)
            ->setDateAdd(new \DateTime())
            ->setCost($product_cost)
            ->setStock($product_stock)
        ;

        if ($row[$keysMapping['product_discon']]!=null) {
            $product
                ->setDateDisc(new \DateTime());
        }
        return $product;
    }
}
