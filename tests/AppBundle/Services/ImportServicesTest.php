<?php
namespace Test\AppBundle\Service;

use AppBundle\Services\EntityPrepareService;
use AppBundle\Services\ImportService;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportServicesTest extends TestCase
{
    /**
     * @var ImportService
     */
    private $importService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $productService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $validator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $logger;
    public function setUp()
    {

        // Create Moc classes
        $this->em = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->validator = $this->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->productService = $this->getMockBuilder(EntityPrepareService::class)
            ->getMock()
        ;

        $this->importService = new ImportService($this->em, $this->validator, $this->logger, $this->productService, 50);
    }

    public function testCheckErrors()
    {
        $reader = Reader::createFromPath('%kernel.root_dir%/../src/AppBundle/Data/stock.csv');
        $csvResult = $reader->fetchAssoc();

        $result = $this->importService->importProducts($csvResult, true);

        //will not be started because product is mock service
        $this->validator
            ->expects($this->never())
            ->method('validate')
        ;
        //will not be started because it is test
        $this->em
            ->expects($this->never())
            ->method('flush')
        ;
        //rows will be 29
        $this->assertSame(29, $result['rows_numbers']);
    }
}
