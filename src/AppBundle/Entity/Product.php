<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 * @ORM\Entity
 * @ORM\Table(name="ProductData")
 * @UniqueEntity("productCode", message="Product Code is used")
 */

class Product
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="productName", type="string", length=50)
     */
    private $productName;

    /**
     * @var string
     *
     * @ORM\Column(name="productDesc", type="string", length=255)
     *php
     */
    private $productDesc;

    /**
     * @var string
     * @ORM\Column(name="productCode", type="string", length=10,unique=true)
     */
    private $productCode;


    /**
     * @var float
     * @ORM\Column(name="productCost",
     *     precision=9,scale=2,type="decimal", columnDefinition="integer unsigned", nullable=false)
     * @Assert\Expression(
     *     "this.getStock() >= 10 or this.getCost()>=5",
     *     message="This product's cost less than 5 and stock less than 10"
     * )
     * @Assert\LessThanOrEqual(1000)
     */
    private $cost;


    /**
     * @ORM\Column(name="stock",type="integer", columnDefinition="integer unsigned", nullable=false)
     * @var int
     */

    private $stock;



    /**
     * @var \DateTime
     * @ORM\Column(name="dateAdd", type="datetime", options=true, nullable=true)
     */

    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDisc", type="datetime",nullable=true)
     */
    private $dateDisc;



    /**
     * @return float
     */
    public function getCost() : float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return Product
     */
    public function setCost(float $cost) : Product
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock() : int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return Product
     */
    public function setStock(int $stock) : Product
    {
        $this->stock = $stock;
        return $this;
    }



    /**
     * @return \DateTime
     */
    public function getDateAdd() : \DateTime
    {
        return $this->dateAdd;
    }

    /**
     * @param \DateTime $dateAdd
     * @return Product
     */
    public function setDateAdd(\DateTime $dateAdd) : Product
    {
        $this->dateAdd = $dateAdd;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateDisc() : \DateTime
    {
        return $this->dateDisc;
    }

    /**
     * @param \DateTime $dateDisc
     * @return Product
     */
    public function setDateDisc(\DateTime $dateDisc) : Product
    {
        $this->dateDisc = $dateDisc;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getProductName() : string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     * @return Product
     */
    public function setProductName(string $productName) : Product
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * @return string
     *
     */
    public function getProductDesc() : string
    {
        return $this->productDesc;
    }

    /**
     * @param string $productDesc
     * @return Product
     */
    public function setProductDesc(string $productDesc) : Product
    {
        $this->productDesc = $productDesc;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductCode() : string
    {
        return $this->productCode;
    }

    /**
     * @param string $productCode
     * @return Product
     */
    public function setProductCode(string  $productCode) : Product
    {
        $this->productCode = $productCode;

        return $this;
    }
}
